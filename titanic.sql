CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
SELECT uuid_generate_v4();

CREATE TABLE people (
  uuid uuid DEFAULT uuid_generate_v4(),
  survived INT NOT NULL,
  passenger_class INT NOT NULL,
  name VARCHAR(255) NOT NULL,
  sex VARCHAR(255) NOT NULL,
  age REAL NOT NULL,
  siblings_or_spouses_aboard INT NOT NULL,
  parents_or_children_aboard INT NOT NULL,
  fare REAL NOT NULL
);
